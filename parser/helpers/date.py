import datetime
import re

get_raw_date = re.compile(r'\d{1,2} [а-я]+ \d{4}')

translating = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июля',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря',
]


def parse_date_starts(time_data: str):
    time_data = get_raw_date.search(time_data)
    if time_data is None:
        return None

    time_data = time_data.group(0)

    for i, m in enumerate(translating, 1):
        if m in time_data:
            time_data = time_data.replace(m, str(i))
            break

    return datetime.datetime.strptime(time_data, u'%d %m %Y')


def generate_time_series(start: datetime, step: datetime.timedelta = datetime.timedelta(days=365 * 8)):
    current = start
    now = datetime.datetime.now()
    while current < datetime.datetime.now():
        next_current = (current + step)
        if now < next_current:
            yield (current, now)
            break
        yield (current, next_current)
        current = next_current + datetime.timedelta(days=1)


def datetime_format(*dt):
    return tuple(x.strftime("%d.%m.%Y") for x in dt)


if __name__ == '__main__':
    for i in generate_time_series(start=datetime.datetime(year=2005, day=2, month=2)):
        print(datetime_format(*i))

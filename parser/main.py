import asyncio
import os
import re
from datetime import datetime
from http.cookies import SimpleCookie
from typing import NamedTuple

import aiofiles
import aiohttp
from lxml import html, etree
from yarl import URL

from helpers.date import parse_date_starts, generate_time_series

date_format = '%d.%m.%Y'
regions_path = 'config/regions.txt'
data_path = '/data'
archives_path = '/shared/archives'

base_url = 'https://rp5.ru'
main_page_url = f'{base_url}/Погода_в_России'

city_page_urls = set()
archive_urls = set()
get_archive_link = re.compile('<a href=(.+?)>')
get_input = etree.XPath('//input[@name=$name]/@value')


class Response(NamedTuple):
    content: bytes
    cookies: SimpleCookie
    url: URL


class ArchiveRequest(NamedTuple):
    wmo_id: str  # = //input[@name="wmo_id"]
    f_ed3: str  # = //input[@name="f_ed3"]
    f_ed4: str  # = //input[@name="f_ed4"]
    f_ed5: str  # = //input[@name="f_ed5"]
    f_pe: str = 1  # for each day day
    f_pe1: str = 2  # UTF-8
    lng_id: str = 2


class ArchiveResponse(NamedTuple):
    start_date: datetime
    end_date: datetime
    link: str


async def get_request(url: str, **kwargs) -> bytes:
    while True:
        try:
            async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
                async with session.get(url) as resp:
                    return await resp.read()
        except BaseException as e:
            print('Error!', url, e)
            await asyncio.sleep(3)


async def saved_request(url, name='other_pages') -> bytes:
    html_dir_path = os.path.join(data_path, name)
    html_path = os.path.join(html_dir_path, url.replace(base_url, '').replace('/', '') + '.html')

    if os.path.exists(html_path):
        async with aiofiles.open(html_path, 'rb') as f:
            return await f.read()

    os.makedirs(html_dir_path, exist_ok=True)
    response = await get_request(url)
    async with aiofiles.open(html_path, 'wb') as f:
        await f.write(response)
    return response


async def get_cities_links(region_url: str):
    get_map_weather_links = etree.XPath('//div[@class="countryMap"]//a[contains(@href, "Погода")]/@href')
    response = await saved_request(base_url + region_url, name='get_cities_links')
    if b'countryMap' in response:
        tree = html.fromstring(response)
        city_or_region_links = get_map_weather_links(tree)
        city_or_region_links = map(lambda y: y if y.startswith('/') else '/' + y, city_or_region_links)
        for city_or_region_link in city_or_region_links:
            async for x in get_cities_links(city_or_region_link):
                yield x
    else:
        yield (response, region_url)


def parse_city_archive_link(content: bytes):
    get_archive_link = etree.XPath('//a[@id="archive_link"]/@href')
    tree = html.fromstring(content)
    archive_link = next(iter(get_archive_link(tree)), None)
    if archive_link is not None and archive_link not in archive_urls:
        archive_urls.add(archive_link)
        return archive_link
    return None


async def get_archive(url: str, path: str, name: str):
    response = await get_request(url.replace('../', ''))
    folder_path = os.path.join(archives_path, path)
    os.makedirs(folder_path, exist_ok=True)

    async with aiofiles.open(os.path.join(folder_path, f'{name}.csv.gz'), 'wb') as f:
        await f.write(response)

    print('save', path, name)


async def get_archive_links(session, info: ArchiveRequest, start: datetime):
    multipart_params = dict(info._asdict())
    for a_date1, a_date2 in generate_time_series(start):
        multipart_params['a_date1'] = a_date1.strftime(date_format)
        multipart_params['a_date2'] = a_date2.strftime(date_format)
        async with await session.post(
                f'{base_url}/responses/reFileSynop.php',
                data=aiohttp.FormData(multipart_params),
                headers={
                    'Referer': 'https://rp5.ru/',
                    'X-Requested-With': 'XMLHttpRequest',
                }
        ) as resp:
            response = await resp.read()

        found_link = get_archive_link.search(str(response))
        if found_link is None:
            continue

        yield ArchiveResponse(
            start_date=a_date1,
            end_date=a_date2,
            link=found_link.group(1)
        )


async def parse_archive(archive_link: str):
    print('got', archive_link)
    session = aiohttp.ClientSession()

    while True:
        try:
            async with session.get(archive_link) as resp:
                response = await resp.read()
                cookies: SimpleCookie = resp.cookies
                cookies.header = 'Cookie'
                link_name = resp.url.name
                break
        except Exception as e:
            print('Error parse_archive', e)

    os.makedirs(os.path.join(data_path, 'parse_archive'), exist_ok=True)
    async with aiofiles.open(os.path.join(data_path, 'parse_archive', link_name + '.html'), 'wb') as f:
        await f.write(response)

    tree = html.fromstring(response)
    get_tree_input = lambda name: get_input(tree, name=name)[0]
    get_time_starts = etree.XPath('//form[@id="fsynop"]/span/text()[2]')

    info = ArchiveRequest(
        wmo_id=get_tree_input(name='wmo_id'),
        f_ed3=get_tree_input(name='f_ed3'),
        f_ed4=get_tree_input(name='f_ed4'),
        f_ed5=get_tree_input(name='f_ed5'),
    )

    # archives = [x ]

    async for archive_response in get_archive_links(
            session,
            info,
            parse_date_starts(get_time_starts(tree)[0])
    ):
        await get_archive(
            url=archive_response.link,
            path=link_name,
            name=f"{archive_response.start_date.strftime(date_format)}-"
                 f"{archive_response.end_date.strftime(date_format)}"
        )

    await session.close()


async def parse_main_page(region):
    get_region_url = etree.XPath('//div[a[text()=$main_city]]/a[last()]/@href')

    response: bytes = await saved_request(main_page_url, name='parse_main_page')
    tree = html.fromstring(response)
    region_url = next(iter(get_region_url(tree, main_city=region)), None)
    if region_url is None:
        print('region_url not found for', region)
        return

    async for city_page_content, city_page_url in get_cities_links(region_url):
        if city_page_url in city_page_urls:
            continue
        city_page_urls.add(city_page_url)

        archive_link = parse_city_archive_link(city_page_content)
        if archive_link is None:
            continue
        await parse_archive(archive_link)


async def main(regions):
    await asyncio.gather(*(parse_main_page(region) for region in regions))


if __name__ == "__main__":
    os.makedirs(archives_path, exist_ok=True)
    with open(regions_path, 'r') as regions_file:
        regions_raw = regions_file.read()

    asyncio.run(main(regions_raw.split('\n')))
    print('done')

# //a[@id="archive_link"]


# go to https://rp5.ru/Архив_погоды_в_Тюмени and get Set-Cookie PHPSESSID=a1725b2f9e90c77dfaa21cc895baeca3;
# //a[span[@class="ToWeather"]]/@href
# //div[@class="countryMap"]//a[contains(@href, 'Погода')] - all links

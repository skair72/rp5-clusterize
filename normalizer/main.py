import csv
import datetime
import gzip
import io
import os

shared_path = '/shared'
archives_path = os.path.join(shared_path, 'archives')
dataset_path = os.path.join(shared_path, 'dataset')


def unzip_and_merge(archives_folder):
    gz_files = sorted((x for x in os.listdir(archives_folder) if not x.startswith('.')),
                      key=lambda x: datetime.datetime.strptime(x.split('-')[0], '%d.%m.%Y'),
                      reverse=True)

    output_filename = f"{archives_folder.split('/')[-1]}__" \
                      f"{gz_files[-1].split('-')[0]}-" \
                      f"{gz_files[0].split('-')[1]}".replace('.gz', '')

    first_file = True
    with open(os.path.join(dataset_path, output_filename), 'w') as output_file:
        csv_writer = csv.writer(output_file)
        for gz_file in gz_files:
            with gzip.open(os.path.join(archives_folder, gz_file), 'rb') as csv_bytes:
                csv_reader = csv.reader(io.TextIOWrapper(csv_bytes), delimiter=';', quotechar='"')

                # Skipping headers
                for _ in range(7 - int(first_file)):
                    next(csv_reader)

                csv_writer.writerows(csv_reader)
            first_file = False


def main():
    archives_folders = (x for x in os.listdir(archives_path) if not x.startswith('.'))
    for archives_folder in archives_folders:
        unzip_and_merge(os.path.join(archives_path, archives_folder))


if __name__ == '__main__':
    os.makedirs(dataset_path, exist_ok=True)
    os.listdir()
    main()

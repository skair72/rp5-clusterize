PROJECT_DIR=$(shell pwd)

parse:
	docker build -t rp5_parser $(PROJECT_DIR)/parser
	docker run \
	-v $(PROJECT_DIR)/parser:/app \
	-v $(PROJECT_DIR)/parser/data:/data \
	-v $(PROJECT_DIR)/shared:/shared \
	--rm -d --name rp5_parser rp5_parser

normalize:
	docker build -t rp5_normalizer $(PROJECT_DIR)/normalizer
	docker run \
	-v $(PROJECT_DIR)/normalizer:/app \
	-v $(PROJECT_DIR)/shared:/shared \
	--rm -d --name rp5_normalizer rp5_normalizer

jupyter:
	docker run -d --name rp5_jupyter \
	-v $(PROJECT_DIR)/notebooks:/home/jovyan/work \
	-v $(PROJECT_DIR)/shared:/home/jovyan/shared \
	-p 8888:8888 \
	jupyter/scipy-notebook:latest
	# docker exec -it rp5_jupyter conda install -c https://conda.anaconda.org/plotly plotly
